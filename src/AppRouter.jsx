import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import AppIndex from './AppIndex';
import AppAbout from './components/AppAbout';
import Products from './components/Products';

class AppRouter extends Component {
    render() {
        return (
            <BrowserRouter>
            <div>
                <Route exact path="/" component={AppIndex}/>
                <Route path="/about" component={AppAbout}/>
                <Route path="/products" component={Products}/>
            </div>
            </BrowserRouter>
        )
    }
}

export default AppRouter;