import React, { Component } from 'react';
// import logo from './logo.svg';
import AppHeader from './components/AppHeader';
import AppCarousel from './components/common/AppCarousel';
// import AppCard from './components/common/Card';
import AppBody from './components/AppBody';
import './App.css';
import Products from './components/Products';

class AppIndex extends Component {
  render() {
    return (
      <div className="App">
        <AppHeader />
        <AppCarousel />
        <AppBody />
        {/* <AppCard /> */}
        <Products />
      </div>
    );
  }
}

export default AppIndex;
