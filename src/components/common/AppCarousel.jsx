import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

// const items = [
//   {
//     src: 'https://images-na.ssl-images-amazon.com/images/I/91McsWOj8sL._SL1500_.jpg',
//     altText: 'Slide 1',
//     caption: 'Slide 1'
//   },
//   {
//     src: 'https://sendelisin.com/wp-content/uploads/2018/09/Marvel-Spider-Man.jpg',
//     altText: 'Slide 2',
//     caption: 'Slide 2'
//   },
//   {
//     src: 'https://cdn.vox-cdn.com/thumbor/3RFyleqF41pO1d9IGxLkK2powa4=/0x0:1920x1080/1200x800/filters:focal(777x249:1083x555)/cdn.vox-cdn.com/uploads/chorus_image/image/61384181/Spider_Man_Screen_Shot_9_10_18__11.54_AM.0.png',
//     altText: 'Slide 3',
//     caption: 'Slide 3'
//   }
// ];

class AppCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.items = [
      {
        src: 'https://3.bp.blogspot.com/-5cUtEAKT-ZU/WMzdsssQZiI/AAAAAAAAEfI/x7OeEOKoLPM-QHVVZPDm1O50sczwGoLYgCLcB/s1600/jasa-desain-flyer-brosur-promosi-perusahaan-perumahan-restoran-rumahmakan-makanan-minuman-kuelebaran-jasa-surabaya-malang--gresik-jambi-pekanbaru-solo-sidoarjo-ujungpandang-kediri-tulungagung-gresik-jakarta-bali-batam.jpg',
        altText: 'Slide 1',
        caption: 'Slide 1'
      },
      {
        src: 'https://cdn.idntimes.com/content-images/post/20181003/d3913b6ab5bce6a0941b2295969ed742.jpg',
        altText: 'Slide 2',
        caption: 'Slide 2'
      },
      {
        src: 'http://pidpid.com/wp-content/uploads/2012/06/Chowking-OC-Counter-Standee_30x45cm-rev-just.jpg',
        altText: 'Slide 3',
        caption: 'Slide 3'
      }
    ];
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;

    const slides = this.items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img src={item.src} alt={item.altText} />
          <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        <CarouselIndicators items={this.items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
    );
  }
}


export default AppCarousel;