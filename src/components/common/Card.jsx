import React, { Component } from 'react';
import { Card, CardImg, CardBody, CardTitle, CardSubtitle } from 'reactstrap';

export default class AppCard extends Component {
    render() {
        return (
            <Card>
                <CardImg top width="100%" src={this.props.image} alt={this.props.alt} />
                    <CardBody>
                        <CardTitle>{ this.props.title }</CardTitle>
                        <CardSubtitle>{this.props.harga}</CardSubtitle>
                    </CardBody>
            </Card>
        )
    }
}