import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

class AppHeader extends Component {
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false,
          products: []
        };
      }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    
    componentDidMount() {
        axios.get(``)
          .then(res => {
            // const content = res.data.content;
            this.setState({ products: res.data });
          })
      }

    render() {
        console.log('Ini data :', this.state.products );
        return (
            <header className="App-Header">
                <Navbar color="light" light expand="md">
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink><Link to='/'>Home</Link></NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink><Link to='/about'>About</Link></NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink><Link to='/Produk'>Products</Link></NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            Perakunan
                        </DropdownToggle>
                        <DropdownMenu right>
                        <DropdownItem>
                            Akun Baru
                        </DropdownItem>
                        <DropdownItem>
                            Masuk Pak EKo
                        </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>
                            Password Ilang
                        </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                    </Nav>
                </Collapse>
                </Navbar>
            </header>
        );
    }
}

export default AppHeader;