import React, { Component } from 'react'
// import AppCarousel from './common/AppCarousel';
import { Container, Row, Col } from 'reactstrap';
import AppCard from './common/Card';
import axios from 'axios';

export default class AppBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    }
  }

  componentDidMount() {
    axios.get('http://reduxblog.herokuapp.com/api/posts?key=wisnu')
      .then(res => {
       
        this.setState({ products: res.data });

      })
  }
  
  render() {
    console.log('ini data', this.state.products)
    const looping = this.state.products.map((prod, index) => {
      return(
        <Col md='4' key={index}>
        <AppCard 
          image={prod.categories}
          title={prod.title}
          harga={prod.content}
        />
        </Col>
      ) 
    }   
    );


    return (
      <div>
        {/* <AppCarousel /> */}
        <Container className="body-content">
        <h2>Produk</h2>
          <Row className='d-flex'>
              {looping}

          </Row>
        </Container>
      </div>
    )
  }
}