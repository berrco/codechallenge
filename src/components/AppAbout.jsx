import React, { Component } from 'react';
import AppHeader from './AppHeader';

class AppAbout extends Component {
    render() {
        return (
            <div>
                <AppHeader />
                <h1>Halaman Tentang Sesuatu</h1>
            </div>
        )
    }
}

export default AppAbout;